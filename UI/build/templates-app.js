angular.module('templates-app', ['OSGi/OSGi.tpl.html', 'about/about.tpl.html', 'connection_manager/discover.tpl.html', 'connection_manager/index.tpl.html', 'connection_manager/root.tpl.html', 'container/create.tpl.html', 'container/detail.tpl.html', 'container/edit.tpl.html', 'container/index.tpl.html', 'container/root.tpl.html', 'flow/composer.tpl.html', 'flow/create.tpl.html', 'flow/detail.tpl.html', 'flow/edit.tpl.html', 'flow/index.tpl.html', 'flow/node.tpl.html', 'flow/root.tpl.html', 'home/home.tpl.html', 'network/index.tpl.html', 'network/root.tpl.html', 'network/staticroutes.create.tpl.html', 'network/staticroutes.edit.tpl.html', 'network/staticroutes.tpl.html', 'network/subnets.create.tpl.html', 'network/subnets.edit.tpl.html', 'network/subnets.tpl.html', 'node/detail.tpl.html', 'node/index.tpl.html', 'node/root.tpl.html', 'span_ports/create.tpl.html', 'span_ports/index.tpl.html', 'span_ports/root.tpl.html', 'topology/topology.tpl.html', 'user/create.tpl.html', 'user/index.tpl.html', 'user/root.tpl.html']);

angular.module("OSGi/OSGi.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("OSGi/OSGi.tpl.html",
    "");
}]);

angular.module("about/about.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("about/about.tpl.html",
    "<div class=\"row-fluid\">\n" +
    "  <h1 class=\"page-header\">\n" +
    "    The Elevator Pitch\n" +
    "    <small>For the lazy and impatient.</small>\n" +
    "  </h1>\n" +
    "  <p>\n" +
    "    <code>ng-boilerplate</code> is an opinionated kickstarter for web\n" +
    "    development projects. It's an attempt to create a simple starter for new\n" +
    "    web sites and apps: just download it and start coding. The goal is to\n" +
    "    have everything you need to get started out of the box; of course it has\n" +
    "    slick styles and icons, but it also has a best practice directory structure\n" +
    "    to ensure maximum code reuse. And it's all tied together with a robust\n" +
    "    build system chock-full of some time-saving goodness.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>Why?</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Because my team and I make web apps, and \n" +
    "    last year AngularJS became our client-side framework of choice. We start\n" +
    "    websites the same way every time: create a directory structure, copy and\n" +
    "    ever-so-slightly tweak some config files from an older project, and yada\n" +
    "    yada, etc., and so on and so forth. Why are we repeating ourselves? We wanted a starting point; a set of\n" +
    "    best practices that we could identify our projects as embodying and a set of\n" +
    "    time-saving wonderfulness, because time is money.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    There are other similar projects out there, but none of them suited our\n" +
    "    needs. Some are awesome but were just too much, existing more as reference\n" +
    "    implementations, when we really just wanted a kickstarter. Others were just\n" +
    "    too little, with puny build systems and unscalable architectures.  So we\n" +
    "    designed <code>ng-boilerplate</code> to be just right.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>What ng-boilerplate Is Not</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    This is not an example of an AngularJS app. This is a kickstarter. If\n" +
    "    you're looking for an example of what a complete, non-trivial AngularJS app\n" +
    "    that does something real looks like, complete with a REST backend and\n" +
    "    authentication and authorization, then take a look at <code><a\n" +
    "        href=\"https://github.com/angular-app/angular-app/\">angular-app</a></code>, \n" +
    "    which does just that, and does it well.\n" +
    "  </p>\n" +
    "\n" +
    "  <h1 class=\"page-header\">\n" +
    "    So What's Included?\n" +
    "    <small>I'll try to be more specific than \"awesomeness\".</small>\n" +
    "  </h1>\n" +
    "  <p>\n" +
    "    This section is just a quick introduction to all the junk that comes\n" +
    "    pre-packaged with <code>ng-boilerplate</code>. For information on how to\n" +
    "    use it, see the <a\n" +
    "      href=\"https://github.com/joshdmiller/ng-boilerplate#readme\">project page</a> at\n" +
    "    GitHub.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    The high-altitude view is that the base project includes \n" +
    "    <a href=\"http://getbootstrap.com\">Twitter Bootstrap</a>\n" +
    "    styles to quickly produce slick-looking responsive web sites and apps. It also\n" +
    "    includes <a href=\"http://angular-ui.github.com/bootstrap\">UI Bootstrap</a>,\n" +
    "    a collection of native AngularJS directives based on the aforementioned\n" +
    "    templates and styles. It also includes <a href=\"http://fortawesome.github.com/Font-Awesome/\">Font Awesome</a>,\n" +
    "    a wicked-cool collection of font-based icons that work swimmingly with all\n" +
    "    manner of web projects; in fact, all images on the site are actually font-\n" +
    "    based icons from Font Awesome. Neat! Lastly, this also includes\n" +
    "    <a href=\"http://joshdmiller.github.com/angular-placeholders\">Angular Placeholders</a>,\n" +
    "    a set of pure AngularJS directives to do client-side placeholder images and\n" +
    "    text to make mocking user interfaces super easy.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    And, of course, <code>ng-boilerplate</code> is built on <a href=\"http://angularjs.org\">AngularJS</a>,\n" +
    "    by the far the best JavaScript framework out there! But if you don't know\n" +
    "    that already, then how did you get here? Well, no matter - just drink the\n" +
    "    Kool Aid. Do it. You know you want to.\n" +
    "  </p>\n" +
    "  \n" +
    "  <h2>Twitter Bootstrap</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    You already know about this, right? If not, <a\n" +
    "      href=\"http://getbootstrap.com\">hop on over</a> and check it out; it's\n" +
    "    pretty sweet. Anyway, all that wonderful stylistic goodness comes built in.\n" +
    "    The LESS files are available for you to import in your main stylesheet as\n" +
    "    needed - no excess, no waste. There is also a dedicated place to override\n" +
    "    variables and mixins to suit your specific needs, so updating to the latest\n" +
    "    version of Bootstrap is as simple as: \n" +
    "  </p>\n" +
    "\n" +
    "  <pre>$ cd vendor/twitter-bootstrap<br />$ git pull origin master</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    Boom! And victory is ours.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>UI Bootstrap</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    What's better than Bootstrap styles? Bootstrap directives!  The fantastic <a href=\"http://angular-ui.github.com/bootstrap\">UI Bootstrap</a>\n" +
    "    library contains a set of native AngularJS directives that are endlessly\n" +
    "    extensible. You get the tabs, the tooltips, the accordions. You get your\n" +
    "    carousel, your modals, your pagination. And <i>more</i>.\n" +
    "    How about a quick demo? \n" +
    "  </p>\n" +
    "\n" +
    "  <ul>\n" +
    "    <li class=\"dropdown\">\n" +
    "      <a class=\"btn dropdown-toggle\">\n" +
    "        Click me!\n" +
    "      </a>\n" +
    "      <ul class=\"dropdown-menu\">\n" +
    "        <li ng-repeat=\"choice in dropdownDemoItems\">\n" +
    "          <a>{{choice}}</a>\n" +
    "        </li>\n" +
    "      </ul>\n" +
    "    </li>\n" +
    "  </ul>\n" +
    "\n" +
    "  <p>\n" +
    "    Oh, and don't include jQuery;  \n" +
    "    you don't need it.\n" +
    "    This is better.\n" +
    "    Don't be one of those people. <sup>*</sup>\n" +
    "  </p>\n" +
    "\n" +
    "  <p><small><sup>*</sup> Yes, there are exceptions.</small></p>\n" +
    "\n" +
    "  <h2>Font Awesome</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Font Awesome has earned its label. It's a set of open (!) icons that come\n" +
    "    distributed as a font (!) for super-easy, lightweight use. Font Awesome \n" +
    "    works really well with Twitter Bootstrap, and so fits right in here.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    There is not a single image on this site. All of the little images and icons \n" +
    "    are drawn through Font Awesome! All it takes is a little class:\n" +
    "  </p>\n" +
    "\n" +
    "  <pre>&lt;i class=\"icon-flag\"&gt;&lt/i&gt</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    And you get one of these: <i class=\"icon-flag\"> </i>. Neat!\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>Placeholders</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Angular Placeholders is a simple library for mocking up text and images. You\n" +
    "    can automatically throw around some \"lorem ipsum\" text:\n" +
    "  </p>\n" +
    "\n" +
    "  <pre>&lt;p ph-txt=\"3s\"&gt;&lt;/p&gt;</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    Which gives you this:\n" +
    "  </p>\n" +
    "\n" +
    "  <div class=\"pre\">\n" +
    "    &lt;p&gt;\n" +
    "    <p ph-txt=\"3s\"></p>\n" +
    "    &lt;/p&gt;\n" +
    "  </div>\n" +
    "\n" +
    "  <p>\n" +
    "    Even more exciting, you can also create placeholder images, entirely \n" +
    "    client-side! For example, this:\n" +
    "  </p>\n" +
    "  \n" +
    "  <pre>\n" +
    "&lt;img ph-img=\"50x50\" /&gt;\n" +
    "&lt;img ph-img=\"50x50\" class=\"img-polaroid\" /&gt;\n" +
    "&lt;img ph-img=\"50x50\" class=\"img-rounded\" /&gt;\n" +
    "&lt;img ph-img=\"50x50\" class=\"img-circle\" /&gt;</pre>\n" +
    "\n" +
    "  <p>\n" +
    "    Gives you this:\n" +
    "  </p>\n" +
    "\n" +
    "  <div>\n" +
    "    <img ph-img=\"50x50\" />\n" +
    "    <img ph-img=\"50x50\" class=\"img-polaroid\" />\n" +
    "    <img ph-img=\"50x50\" class=\"img-rounded\" />\n" +
    "    <img ph-img=\"50x50\" class=\"img-circle\" />\n" +
    "  </div>\n" +
    "\n" +
    "  <p>\n" +
    "    Which is awesome.\n" +
    "  </p>\n" +
    "\n" +
    "  <h1 class=\"page-header\">\n" +
    "    The Roadmap\n" +
    "    <small>Really? What more could you want?</small>\n" +
    "  </h1>\n" +
    "\n" +
    "  <p>\n" +
    "    This is a project that is <i>not</i> broad in scope, so there's not really\n" +
    "    much of a wish list here. But I would like to see a couple of things:\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    I'd like it to be a little simpler. I want this to be a universal starting\n" +
    "    point. If someone is starting a new AngularJS project, she should be able to\n" +
    "    clone, merge, or download its source and immediately start doing what she\n" +
    "    needs without renaming a bunch of files and methods or deleting spare parts\n" +
    "    ... like this page. This works for a first release, but I just think there\n" +
    "    is a little too much here right now.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    I'd also like to see a simple generator. Nothing like <a href=\"yeoman.io\">\n" +
    "    Yeoman</a>, as there already is one of those, but just something that\n" +
    "    says \"I want Bootstrap but not Font Awesome and my app is called 'coolApp'.\n" +
    "    Gimme.\" Perhaps a custom download builder like UI Bootstrap\n" +
    "    has. Like that. Then again, perhaps some Yeoman generators wouldn't be out\n" +
    "    of line. I don't know. What do you think?\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    Naturally, I am open to all manner of ideas and suggestions. See the \"Can I\n" +
    "    Help?\" section below.\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>The Tactical To Do List</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    There isn't much of a demonstration of UI Bootstrap. I don't want to pollute\n" +
    "    the code with a demo for demo's sake, but I feel we should showcase it in\n" +
    "    <i>some</i> way.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    <code>ng-boilerplate</code> should include end-to-end tests. This should\n" +
    "    happen soon. I just haven't had the time.\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    Lastly, this site should be prettier, but I'm no web designer. Throw me a\n" +
    "    bone here, people!\n" +
    "  </p>\n" +
    "\n" +
    "  <h2>Can I Help?</h2>\n" +
    "\n" +
    "  <p>\n" +
    "    Yes, please!\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    This is an opinionated kickstarter, but the opinions are fluid and\n" +
    "    evidence-based. Don't like the way I did something? Think you know of a\n" +
    "    better way? Have an idea to make this more useful? Let me know! You can\n" +
    "    contact me through all the usual channels or you can open an issue on the\n" +
    "    GitHub page. If you're feeling ambitious, you can even submit a pull\n" +
    "    request - how thoughtful of you!\n" +
    "  </p>\n" +
    "\n" +
    "  <p>\n" +
    "    So join the team! We're good people.\n" +
    "  </p>\n" +
    "</div>\n" +
    "\n" +
    "");
}]);

angular.module("connection_manager/discover.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("connection_manager/discover.tpl.html",
    "<h3>Discover a new Node (Switch)</h3>\n" +
    "<form role=\"form\" name=\"discoverForm\" ng-submit=\"do\">\n" +
    "    <div class=\"form-inline\">\n" +
    "        <div class=\"form-group\">\n" +
    "            <label for=\"nodeId\" class=\"control-label\">ID / Name</label>\n" +
    "            <input type=\"text\" class=\"form-control input-sm\" id=\"nodeId\" ng-model=\"nodeId\" placeholder=\"Example: management-switch01\" required>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"form-group\">\n" +
    "            <label for=\"nodeAddress\" class=\"control-label\">Address</label>\n" +
    "            <input type=\"text\" class=\"form-control input-sm\" id=\"nodeAddress\" ng-model=\"nodeAddress\" placeholder=\"Example: 10.0.0.1\" required>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"form-group\">\n" +
    "            <label for=\"nodePort\" class=\"control-label\">Port</label>\n" +
    "            <input type=\"text\" class=\"form-control input-sm\" id=\"nodePort\" ng-model=\"nodePort\" required>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <button class=\"btn btn-primary btn-xs\" ng-disabled=\"discoverForm.$invalid\" ng-click=\"doDiscover()\">Discover Node</button>\n" +
    "</form>\n" +
    "\n" +
    "<br/>\n" +
    "<span ng-if=\"error\" class=\"alert alert-danger\">Error: {{error}}</span>");
}]);

angular.module("connection_manager/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("connection_manager/index.tpl.html",
    "<!--<ctrl-reload service=\"svc\"></ctrl-reload>\n" +
    "<show-selected data=\"gridOptions.selectedItems\"></show-selected>\n" +
    "<div class=\"indexGrid\" ng-grid=\"gridOptions\"></div>-->\n" +
    "\n" +
    "\n" +
    "\n" +
    "<table class=\"footable table\">\n" +
    "	<thead>\n" +
    "		<tr>\n" +
    "			<th>ID</th>\n" +
    "			<th data-hide=\"phone\">Type</th>\n" +
    "		</tr>\n" +
    "	</thead>\n" +
    "	<tbody>\n" +
    "		<tr ng-repeat=\"item in data | filter:search\">\n" +
    "        <td>{{item.id}}</td>\n" +
    "        <td>{{item.type}}</td>\n" +
    "      </tr>\n" +
    "	</tbody>\n" +
    "</table>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/javascript\">\n" +
    "	$('.footable').footable();\n" +
    "</script>");
}]);

angular.module("connection_manager/root.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("connection_manager/root.tpl.html",
    "<div class=\"menu\">\n" +
    "  <ul class=\"nav nav-pills\">\n" +
    "  	<li ng-class=\"{ active: isState('connection_manager.index') }\"><a href=\"index.html#/connection_manager/index\">Home</a></li>\n" +
    "    <li ng-class=\"{ active: isState('connection_manager.discover') }\"><a href=\"index.html#/connection_manager/discover\">Discover</a></li>\n" +
    "  </ul>\n" +
    "\n" +
    "  <!-- For child state use-->\n" +
    "  <div ui-view=\"submenu\"></div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"main\" ui-view></div>\n" +
    "");
}]);

angular.module("container/create.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("container/create.tpl.html",
    "<form role=\"form\" name=\"createForm\">\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"name\">Name</label>\n" +
    "    <input type=\"text\" class=\"form-control input-sm control-label\" ng-model=\"data.container\" id=\"name\" placeholder=\"Container name\" required>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"nodes\">Nodes</label>\n" +
    "    <select class=\"form-control input-sm\" id=\"nodes\" multiple ui-select2 ng-model=\"currentNodes\">\n" +
    "      <option ng-repeat=\"np in nodeProperties\" value=\"{{np.node.type}}|{{np.node.id}}\">{{np.properties.description.value}}: {{np.node.type}}/{{np.node.id}}</option>\n" +
    "    </select>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"connectors\">Node Connectors</label>\n" +
    "    <select class=\"form-control input-sm\" id=\"connectors\" multiple ui-select2 ng-model=\"data.nodeConnectors\" ng-disabled=\"currentNodes == 0\">\n" +
    "      <optgroup ng-repeat=\"(nodeString, nodeConnectors) in connectorProperties\" label=\"{{nodeString}}\">\n" +
    "        <option ng-repeat=\"connector in nodeConnectors | noRootPorts\" value=\"{{connector.nodeconnector.type}}|{{connector.nodeconnector.id}}@{{nodeString}}\">{{connector.properties.name.value}}</option>\n" +
    "      </optgroup>\n" +
    "    </select>\n" +
    "  </div>\n" +
    "\n" +
    "\n" +
    "  <button-submit form=\"createForm\" function=\"submit\"></button-submit>\n" +
    "  <button-cancel state=\"container.index\"></button-cancel>\n" +
    "  <span class=\"error clearfix\">{{ error }}</span>\n" +
    "</form>");
}]);

angular.module("container/detail.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("container/detail.tpl.html",
    "<h3>Container: {{data['container-config'][0].container}}</h3>\n" +
    "\n" +
    "<h4>Connectors</h4>\n" +
    "<ul>\n" +
    "    <li class=\"white-detail-page\" ng-repeat=\"c in data.containerConfig[0].nodeConnectors\">\n" +
    "        {{c}}\n" +
    "    </li>\n" +
    "</ul>");
}]);

angular.module("container/edit.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("container/edit.tpl.html",
    "<form role=\"form\" name=\"createForm\">\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"name\">Name</label>\n" +
    "    <input disabled type=\"text\" class=\"form-control input-sm control-label\" ng-model=\"data.container\" id=\"name\" placeholder=\"Container name\" required>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"nodes\">Nodes</label>\n" +
    "    <select class=\"form-control input-sm\" id=\"nodes\" multiple ui-select2 ng-model=\"currentNodes\">\n" +
    "      <option ng-repeat=\"np in nodeProperties\" value=\"{{np.node.type}}|{{np.node.id}}\">{{np.properties.description.value}}: {{np.node.type}}/{{np.node.id}}</option>\n" +
    "    </select>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"connectors\">Node Connectors</label>\n" +
    "    <select class=\"form-control input-sm\" id=\"connectors\" multiple ui-select2 ng-model=\"data.nodeConnectors\" ng-disabled=\"currentNodes == 0\">\n" +
    "      <optgroup ng-repeat=\"(nodeString, nodeConnectors) in connectorProperties\" label=\"{{nodeString}}\">\n" +
    "        <option ng-repeat=\"connector in nodeConnectors | noRootPorts\" value=\"{{connector.nodeconnector.type}}|{{connector.nodeconnector.id}}@{{nodeString}}\">{{connector.properties.name.value}}</option>\n" +
    "      </optgroup>\n" +
    "    </select>\n" +
    "  </div>\n" +
    "\n" +
    "\n" +
    "  <button-submit form=\"createForm\" function=\"submit\"></button-submit>\n" +
    "  <button-cancel state=\"container.index\"></button-cancel>\n" +
    "  <span class=\"error clearfix\">{{ error }}</span>\n" +
    "</form>");
}]);

angular.module("container/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("container/index.tpl.html",
    "\n" +
    "<!--<div class=\"indexGrid\" ng-grid=\"gridOptions\"></div>-->\n" +
    "<a class=\"btn btn-info add-row\" href=\"index.html#/container/create\">\n" +
    "    Add a Container\n" +
    "</a>\n" +
    "\n" +
    "<table class=\"footable table\">\n" +
    "	<thead>\n" +
    "		<tr>\n" +
    "			<th>Name</th>\n" +
    "			<th data-hide=\"phone\">Connector Count</th>\n" +
    "		</tr>\n" +
    "	</thead>\n" +
    "	<tbody>\n" +
    "		<tr ng-repeat=\"item in data | filter:search\">\n" +
    "        <td><a href=\"index.html#/container/{{item.container}}/detail\">{{item.container}}</a></td>\n" +
    "        <td>{{item.nodeConnectors.length}}</td>\n" +
    "        <!--<td><a href=\"index.html#/container/{{item.container}}/edit\"><i class=\"icon-edit\"></i></a></td>-->\n" +
    "        <td>\n" +
    "        	<a class=\"row-delete\" href=\"#\">\n" +
    "    			<i class=\"icon-remove\"></i>\n" +
    "			</a>\n" +
    "		</td>\n" +
    "      </tr>\n" +
    "	</tbody>\n" +
    "</table>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/javascript\">\n" +
    "	$('.footable').footable();\n" +
    "</script>");
}]);

angular.module("container/root.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("container/root.tpl.html",
    "<div class=\"menu\">\n" +
    "  <ul class=\"nav nav-pills\">\n" +
    "  	<li ng-class=\"{ active: isState('container.index') || isState('container.detail')  }\"><a href=\"index.html#/container/index\">Home</a></li>\n" +
    "    <li ng-class=\"{ active: isState('container.create') }\"><a href=\"index.html#/container/create\">Create</a></li>\n" +
    "  </ul>\n" +
    "\n" +
    "  <!-- For child state use-->\n" +
    "  <div ui-view=\"submenu\"></div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"main\" ui-view></div>\n" +
    "");
}]);

angular.module("flow/composer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("flow/composer.tpl.html",
    "Flow Composition\n" +
    "<!--<div>\n" +
    "  <div class=\"well well-sm\" ng-repeat=\"(key, value) in chosedOptions\">\n" +
    "    <div ng-switch=\"value\">\n" +
    "      <div ng-switch-default>\n" +
    "        {{value}}\n" +
    "      </div>\n" +
    "    </div>\n" +
    "    {{key}}\n" +
    "  </div>\n" +
    "</div>\n" +
    "-->\n" +
    "\n" +
    "<table class=\"footable table\">\n" +
    "	<thead>\n" +
    "		<tr>\n" +
    "			<th data-toggle=\"true\">Name</th>\n" +
    "			<th>Property</th>\n" +
    "		</tr>\n" +
    "	</thead>\n" +
    "	<tbody>\n" +
    "		<tr ng-repeat=\"item in chosedOptions\">\n" +
    "			<td>{{item.displayName}}</td>\n" +
    "	        <td>{{item.value}}</td>\n" +
    "	        <td>\n" +
    "	        	<a class=\"row-delete\" href=\"\">\n" +
    "	    			<i class=\"icon-remove\"></i>\n" +
    "				</a>\n" +
    "			</td>\n" +
    "      	</tr>\n" +
    "	</tbody>\n" +
    "</table>\n" +
    "");
}]);

angular.module("flow/create.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("flow/create.tpl.html",
    "  <form role=\"form\" name=\"createForm\" style=\"width: 100%\">\n" +
    "    <div class=\"col-md-4\">\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"name\">Name</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm control-label\" ng-model=\"flow.name\" id=\"name\" placeholder=\"Flow name\" required>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"installInHw\">Enabled</label>\n" +
    "        <input type=\"checkbox\" id=\"installInHw\" ng-model=\"flow.installInHw\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"node\">Node</label>\n" +
    "        <select class=\"form-control input-sm\" id=\"node\" ui-select2 ng-model=\"nodeString\" ng-required=\"true\" data-placeholder=\"Select a node\">\n" +
    "          <option value=\"\"></option>\n" +
    "          <option ng-repeat=\"np in nodes.nodeProperties\" value=\"{{np.node.type}}/{{np.node.id}}\">{{np.properties.description.value}}: {{np.node.type}}/{{np.node.id}}</option>\n" +
    "        </select>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"ingressPort\">Ingress Port</label>\n" +
    "        <select class=\"form-control input-sm\" id=\"ingressPort\" ui-select2 ng-model=\"flow.ingressPort\"  ng-required=\"true\" ng-disabled=\"!connectors\" data-placeholder=\"Select ingress port\">\n" +
    "          <option value=\"\"></option>\n" +
    "          <option ng-repeat=\"ncp in connectors.nodeConnectorProperties | noRootPorts | orderBy:'properties.name.value'\" value=\"{{ncp.nodeconnector.id}}\">{{ncp.properties.name.value}}</option>\n" +
    "        </select>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"priority\">Priority</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.priority\" id=\"priority\" placeholder=\"Flow Priority\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"hardTimeout\">Hard Timeout</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.hardTimeout\" id=\"hardTimeout\" placeholder=\"Hard Timeout\">\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"idleTimeout\">Idle Timeout</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.idleTimeout\" id=\"idleTimeout\" placeholder=\"Idle Timeout\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"cookie\">Cookie</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.cookie\" id=\"cookie\" placeholder=\"Cookie\">\n" +
    "      </div>\n" +
    "\n" +
    "      <h3>Layer 2</h3>\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"etherType\">Ehternet Type</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.etherType\" id=\"etherType\" placeholder=\"Ethernet Type\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"vlanPriority\">VLAN Priority</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.vlanPriority\" id=\"vlanPriority\" placeholder=\"VLAN Priority\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"dlSrc\">Source MAC Address</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.dlSrc\" id=\"dlSrc\" placeholder=\"Source MAC Address\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"dlDst\">Destination MAC Address</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.dlDst\" id=\"dlDst\" placeholder=\"Destination MAC Address\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"nwSrc\">Source IP</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.nwSrc\" id=\"nwSrc\" placeholder=\"Source IP\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"nwDst\">Destination IP</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.nwDst\" id=\"nwDst\" placeholder=\"Destination IP\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"tosBits\">TOS Bits</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.tosBits\" id=\"tosBits\" placeholder=\"TOS Bits\">\n" +
    "      </div>\n" +
    "\n" +
    "      <h3>Layer 3</h3>\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"tpSrc\">Source Port</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.tpSrc\" placeholder=\"Source Port\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"tpDst\">Destination Port</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.tpDst\" placeholder=\"Destination Port\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"protocol\">Protocol</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.protocol\" id=\"protocol\" placeholder=\"Protocol (Example: TCP)\">\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"col-md-4\">\n" +
    "    <h3>Actions</h3>\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"actions\">Actions</label>\n" +
    "      <select ng-init=\"actionOptions[0]\" class=\"form-control input-sm\" id=\"actions\" ng-click=\"changeOptions(actionActive)\" ng-model=\"actionActive\" ui-select2>\n" +
    "        <option ng-repeat=\"(key, value) in actionOptions\" ng-show=\"!value.hidden\">{{value.displayName}}</option>\n" +
    "      </select>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"col-md-6\">\n" +
    "    <div ui-view=\"composer\"></div>\n" +
    "\n" +
    "    <button-submit form=\"createForm\" function=\"submit\"></button-submit>\n" +
    "    <button-cancel state=\"flows.index\"></button-cancel>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "  </form>\n" +
    "");
}]);

angular.module("flow/detail.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("flow/detail.tpl.html",
    "<div class=\"col-md-5 detail-page\">\n" +
    "  <h3>{{flow.name}} @ {{flow.node.type}}/{{flow.node.id}}</h3>\n" +
    "\n" +
    "  <table class=\"table table-condensed table-bordered\">\n" +
    "    <thead>\n" +
    "      <tr>\n" +
    "        <th>Is Installed</th>\n" +
    "        <th>Priority</th>\n" +
    "        <th>Hard Timeout</th>\n" +
    "        <th>Idle Timeout</th>\n" +
    "    </thead>\n" +
    "    <tbody>\n" +
    "      <tr>\n" +
    "        <td>{{flow.installInHw}}</td>\n" +
    "        <td>{{flow.priority}}</td>\n" +
    "        <td>{{flow.hardTimeout}}</td>\n" +
    "        <td>{{flow.idleTimeout}}</td>\n" +
    "      </tr>\n" +
    "    </tbody>\n" +
    "  </table>\n" +
    "\n" +
    "  <h4>Match</h4>\n" +
    "  <table class=\"table table-condensed table-bordered\">\n" +
    "    <thead>\n" +
    "      <tr>\n" +
    "        <th>Source MAC</th>\n" +
    "        <th>Destination MAC</th>\n" +
    "        <th>Source IP</th>\n" +
    "        <th>Destination IP</th>\n" +
    "        <th>TOS</th>\n" +
    "        <th>Source Port</th>\n" +
    "        <th>Destination Port</th>\n" +
    "        <th>Protocol</th>\n" +
    "        <th>Cookie</th>\n" +
    "      </tr>\n" +
    "    </thead>\n" +
    "    <tbody>\n" +
    "      <tr>\n" +
    "        <td>{{flow.dlSrc}}</td>\n" +
    "        <td>{{flow.dlDst}}</td>\n" +
    "        <td>{{flow.nwSrc}}</td>\n" +
    "        <td>{{flow.nwDst}}</td>\n" +
    "        <td>{{flow.tosBits}}</td>\n" +
    "        <td>{{flow.tpSrc}}</td>\n" +
    "        <td>{{flow.tpDst}}</td>\n" +
    "        <td>{{flow.protocol}}</td>\n" +
    "        <td>{{flow.cookie}}</td>\n" +
    "      </tr>\n" +
    "    </tbody>\n" +
    "  </table>\n" +
    "\n" +
    "  <h4>VLAN</h4>\n" +
    "  <table class=\"table table-condensed table-bordered\">\n" +
    "    <thead>\n" +
    "      <tr>\n" +
    "        <th>Identifier</th>\n" +
    "        <th>Priority</th>\n" +
    "    </thead>\n" +
    "    <tbody>\n" +
    "      <tr>\n" +
    "        <td>{{flow.vlanId}}</td>\n" +
    "        <td>{{flow.vlanPriority}}</td>\n" +
    "      </tr>\n" +
    "    </tbody>\n" +
    "  </table>\n" +
    "\n" +
    "  <h4>Actions</h4>\n" +
    "  <span>{{flow.actions}}</span>\n" +
    "</div>\n" +
    "");
}]);

angular.module("flow/edit.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("flow/edit.tpl.html",
    "  <form role=\"form\" name=\"createForm\" style=\"width: 100%\">\n" +
    "    <div class=\"col-md-4\">\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"name\">Name</label>\n" +
    "        <input disabled type=\"text\" class=\"form-control input-sm control-label\" ng-model=\"flow.name\" id=\"name\" placeholder=\"Flow name\" required>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"installInHw\">Enabled</label>\n" +
    "        <input type=\"checkbox\" id=\"installInHw\" ng-model=\"flow.installInHw\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"node\">Node</label>\n" +
    "        <select class=\"form-control input-sm\" id=\"node\" ui-select2 ng-model=\"nodeString\" ng-required=\"true\" data-placeholder=\"Select a node\">\n" +
    "          <option value=\"\"></option>\n" +
    "          <option ng-repeat=\"np in nodes.nodeProperties\" value=\"{{np.node.type}}/{{np.node.id}}\">{{np.properties.description.value}}: {{np.node.type}}/{{np.node.id}}</option>\n" +
    "        </select>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"ingressPort\">Ingress Port</label>\n" +
    "        <select class=\"form-control input-sm\" id=\"ingressPort\" ui-select2 ng-model=\"flow.ingressPort\"  ng-required=\"true\" ng-disabled=\"!connectors\" data-placeholder=\"Select ingress port\">\n" +
    "          <option value=\"\"></option>\n" +
    "          <option ng-repeat=\"ncp in connectors.nodeConnectorProperties | noRootPorts | orderBy:'properties.name.value'\" value=\"{{ncp.nodeconnector.id}}\">{{ncp.properties.name.value}}</option>\n" +
    "        </select>\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"priority\">Priority</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.priority\" id=\"priority\" placeholder=\"Flow Priority\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"hardTimeout\">Hard Timeout</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.hardTimeout\" id=\"hardTimeout\" placeholder=\"Hard Timeout\">\n" +
    "      </div>\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"idleTimeout\">Idle Timeout</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.idleTimeout\" id=\"idleTimeout\" placeholder=\"Idle Timeout\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"cookie\">Cookie</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.cookie\" id=\"cookie\" placeholder=\"Cookie\">\n" +
    "      </div>\n" +
    "\n" +
    "      <h3>Layer 2</h3>\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"etherType\">Ehternet Type</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.etherType\" id=\"etherType\" placeholder=\"Ethernet Type\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"vlanPriority\">VLAN Priority</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.vlanPriority\" id=\"vlanPriority\" placeholder=\"VLAN Priority\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"dlSrc\">Source MAC Address</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.dlSrc\" id=\"dlSrc\" placeholder=\"Source MAC Address\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"dlDst\">Destination MAC Address</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.dlDst\" id=\"dlDst\" placeholder=\"Destination MAC Address\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"nwSrc\">Source IP</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.nwSrc\" id=\"nwSrc\" placeholder=\"Source IP\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"nwDst\">Destination IP</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.nwDst\" id=\"nwDst\" placeholder=\"Destination IP\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"tosBits\">TOS Bits</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.tosBits\" id=\"tosBits\" placeholder=\"TOS Bits\">\n" +
    "      </div>\n" +
    "\n" +
    "      <h3>Layer 3</h3>\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"tpSrc\">Source Port</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.tpSrc\" placeholder=\"Source Port\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"tpDst\">Destination Port</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.tpDst\" placeholder=\"Destination Port\">\n" +
    "      </div>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <label for=\"protocol\">Protocol</label>\n" +
    "        <input type=\"text\" class=\"form-control input-sm\" ng-model=\"flow.protocol\" id=\"protocol\" placeholder=\"Protocol (Example: TCP)\">\n" +
    "      </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"col-md-4\">\n" +
    "    <h3>Actions</h3>\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"actions\">Actions</label>\n" +
    "      <select ng-init=\"actionOptions[0]\" class=\"form-control input-sm\" id=\"actions\" ng-click=\"changeOptions(actionActive)\" ng-model=\"actionActive\" ui-select2>\n" +
    "        <option ng-repeat=\"(key, value) in actionOptions\" ng-show=\"!value.hidden\">{{value.displayName}}</option>\n" +
    "      </select>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"col-md-6\">\n" +
    "    <div ui-view=\"composer\"></div>\n" +
    "\n" +
    "    <button-submit form=\"createForm\" function=\"submit\"></button-submit>\n" +
    "    <button-cancel state=\"flows.index\"></button-cancel>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "  </form>\n" +
    "");
}]);

angular.module("flow/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("flow/index.tpl.html",
    "<a class=\"btn btn-info add-row btn-orange\" href=\"index.html#/flow/create\">\n" +
    "    Add New Flow\n" +
    "</a>\n" +
    "\n" +
    "<table class=\"footable table\" data-page-size=\"5\" page-navigation=\"pagination\">\n" +
    "	<thead>\n" +
    "		<tr>\n" +
    "			<th data-toggle=\"true\">Name</th>\n" +
    "			<th data-hide=\"phone\">Install</th>\n" +
    "			<th data-hide=\"phone\" data-type=\"numeric\">Priority</th>\n" +
    "			<th data-hide=\"all\" data-ignore=\"true\">Node ID</th>\n" +
    "			<th data-hide=\"all\" data-ignore=\"true\">Node Type</th>\n" +
    "		</tr>\n" +
    "	</thead>\n" +
    "	<tbody>\n" +
    "		<tr ng-repeat=\"item in data.flowConfig | filter:search\" on-finish-render>\n" +
    "	        <td><a href=\"index.html#/flow/{{item.node.type}}/{{item.node.id}}/{{item.name}}/detail\">{{item.name}}</a></td>\n" +
    "	        <td>{{item.installInHw}}</td>\n" +
    "	        <td>{{item.priority}}&nbsp;</td>\n" +
    "	        <td>{{item.node.id}}</td>\n" +
    "	        <td>{{item.node.type}}</td>\n" +
    "	        <!--<td><a href=\"index.html#/flow/{{item.node.type}}/{{item.node.id}}/{{item.name}}/edit\"><i class=\"icon-edit\"></i></a></td>-->\n" +
    "	        <td>\n" +
    "	        	<a class=\"row-delete\" href=\"#\">\n" +
    "	    			<i class=\"icon-remove\"></i>\n" +
    "				</a>\n" +
    "			</td>\n" +
    "      	</tr>\n" +
    "	</tbody>\n" +
    "	<tfoot class=\"hide-if-no-paging\">\n" +
    "		<tr>\n" +
    "			<td colspan=\"5\">\n" +
    "				<div class=\"pagination pagination-centered\"></div>\n" +
    "			</td>\n" +
    "		</tr>\n" +
    "	</tfoot>\n" +
    "</table>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "<script type=\"text/javascript\">\n" +
    "		\n" +
    "		$('.footable').footable();\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "<!--<ctrl-reload service=\"svc\"></ctrl-reload>\n" +
    "<ctrl-delete service=\"svc\"></ctrl-delete>\n" +
    "<show-selected data=\"gridOptions.selectedItems\"></show-selected>\n" +
    "<div class=\"indexGrid\" ng-grid=\"gridOptions\"></div>\n" +
    "-->");
}]);

angular.module("flow/node.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("flow/node.tpl.html",
    "    <div class=\"col-md-3\">\n" +
    "      <div class=\"well well-sm\">Click on a flow to view details</div>\n" +
    "\n" +
    "      <table class=\"table table-condensed table-hover\">\n" +
    "        <thead><tr><th>Name</th></tr></thead>\n" +
    "        <tbody>\n" +
    "          <tr ng-repeat=\"flow in flows\" ng-style=\"{'cursor': 'pointer'}\" ng-click=\"$state.transitionTo('flows.details', {nodeType: $stateParams.nodeType, nodeId: $stateParams.nodeId, flowName: flow.name})\">\n" +
    "            <td>{{flow.name}}</td>\n" +
    "          </tr>\n" +
    "        </tbody>\n" +
    "      </table>\n" +
    "    </div>\n" +
    "");
}]);

angular.module("flow/root.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("flow/root.tpl.html",
    "<div class=\"menu\">\n" +
    "  <ul class=\"nav nav-pills\">\n" +
    "  	<li ng-class=\"{ active: isState('flow.index') || isState('flow.detail') }\"><a href=\"index.html#/flow/index\">Home</a></li>\n" +
    "    <li ng-class=\"{ active: isState('flow.create') }\"><a href=\"index.html#/flow/create\">Create</a></li>\n" +
    "  </ul>\n" +
    "  <!-- For child state use-->\n" +
    "  <div ui-view=\"submenu\"></div>\n" +
    "</div>\n" +
    "\n" +
    "<div ui-view class=\"main\"></div>\n" +
    "");
}]);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<div>\n" +
    "    This is home page\n" +
    "</div>");
}]);

angular.module("network/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("network/index.tpl.html",
    "<h3>Network Page</h3>\n" +
    "");
}]);

angular.module("network/root.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("network/root.tpl.html",
    "<div class=\"menu\">\n" +
    "  <ul class=\"nav nav-pills\">\n" +
    "    <li ng-class=\"{ active: isState('network.staticroutes') }\"><a href=\"index.html#/network/staticroute\">Static Routes</a></li>\n" +
    "    <li ng-class=\"{ active: isState('network.subnets') }\"><a href=\"index.html#/network/subnet\">Subnets</a></li>\n" +
    "  </ul>\n" +
    "\n" +
    "  <!-- For child state use-->\n" +
    "  <div ui-view=\"submenu\"></div>\n" +
    "</div>\n" +
    "\n" +
    "<div ui-view class=\"main\"></div>\n" +
    "");
}]);

angular.module("network/staticroutes.create.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("network/staticroutes.create.tpl.html",
    "<div class=\"col-md-4\">\n" +
    " <form role=\"form\" name=\"createForm\">\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"name\">Name</label>\n" +
    "      <input type=\"text\" class=\"form-control input-sm\" ng-model=\"data.name\" id=\"name\" placeholder=\"Name\" required>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"prefix\">Network / Mask</label>\n" +
    "      <input type=\"text\" class=\"form-control input-sm\" ng-model=\"data.prefix\" id=\"prefix\" placeholder=\"Network / Mask - Example: 10.0.0.0/24\" required>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"nextHop\">Next Hop</label>\n" +
    "      <input type=\"text\" class=\"form-control input-sm\" ng-model=\"data.nextHop\" id=\"name\" placeholder=\"Next Hop - Example: 10.0.0.1\" required>\n" +
    "    </div>\n" +
    "\n" +
    "    <button-submit form=\"createForm\" function=\"submit\"></button-submit>\n" +
    "    <button-cancel state=\"network.staticroutes\"></button-cancel>\n" +
    "    <span class=\"error clearfix\">{{ error }}</span>\n" +
    "  </form>\n" +
    "\n" +
    "</div>\n" +
    "");
}]);

angular.module("network/staticroutes.edit.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("network/staticroutes.edit.tpl.html",
    "<div class=\"col-md-4\">\n" +
    " <form role=\"form\" name=\"createForm\">\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"name\">Name</label>\n" +
    "      <input disabled type=\"text\" class=\"form-control input-sm\" ng-model=\"data.name\" id=\"name\" placeholder=\"Name\" required>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"prefix\">Network / Mask</label>\n" +
    "      <input type=\"text\" class=\"form-control input-sm\" ng-model=\"data.prefix\" id=\"prefix\" placeholder=\"Network / Mask - Example: 10.0.0.0/24\" required>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"nextHop\">Next Hop</label>\n" +
    "      <input type=\"text\" class=\"form-control input-sm\" ng-model=\"data.nextHop\" id=\"name\" placeholder=\"Next Hop - Example: 10.0.0.1\" required>\n" +
    "    </div>\n" +
    "\n" +
    "    <button-submit form=\"createForm\" function=\"submit\"></button-submit>\n" +
    "    <button-cancel state=\"network.staticroutes\"></button-cancel>\n" +
    "    <span class=\"error clearfix\">{{ error }}</span>\n" +
    "  </form>\n" +
    "\n" +
    "</div>\n" +
    "");
}]);

angular.module("network/staticroutes.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("network/staticroutes.tpl.html",
    "<!--<div class=\"col-md-10\">\n" +
    "  <div class=\"well well-sm\">\n" +
    "    <a href=\"index.html#/network/staticroute/create\" class=\"btn btn-xs btn-primary\"><i class=\"icon-plus-sign\"></i> Create</a>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\"form-group\">\n" +
    "    <div class=\"form-inline\">\n" +
    "      <label class=\"control-label\" for=\"search\">Search</label>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <input class=\"form-control input-sm\" id=\"search\" placeholder=\"Search\" type=\"text\" ng-model=\"search\">\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "\n" +
    "  <table class=\"table table-bordered table-condensed\">\n" +
    "    <thead>\n" +
    "      <tr>\n" +
    "        <th>Name</th>\n" +
    "        <th>Prefix</th>\n" +
    "        <th>Nexthop</th>\n" +
    "      </tr>\n" +
    "    </thead>\n" +
    "    <tbody>\n" +
    "      <tr ng-repeat=\"sr in data.staticRoute | filter:search\">\n" +
    "        <td>{{sr.name}}</td>\n" +
    "        <td>{{sr.prefix}}</td>\n" +
    "        <td>{{sr.nextHop}}</td>\n" +
    "      </tr>\n" +
    "    </tbody>\n" +
    "  </table>\n" +
    "</div>-->\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "<a class=\"btn btn-info add-row\" href=\"index.html#/network/staticroute/create\">\n" +
    "    Add Static Route\n" +
    "</a>\n" +
    "\n" +
    "<table id=\"tblFootable\" class=\"footable table\" data-page-size=\"5\" page-navigation=\"pagination\">\n" +
    "  <thead>\n" +
    "    <tr>\n" +
    "        <th>Name</th>\n" +
    "        <th data-hide=\"phone\">Prefix</th>\n" +
    "        <th data-hide=\"phone\">Nexthop</th>\n" +
    "    </tr>\n" +
    "  </thead>\n" +
    "  <tbody>\n" +
    "    <tr ng-repeat=\"sr in data.staticRoute | filter:search\" on-finish-render>\n" +
    "        <td>{{sr.name}}</td>\n" +
    "        <td>{{sr.prefix}}</td>\n" +
    "        <td>{{sr.nextHop}}</td>\n" +
    "        <td><a href=\"index.html#/network/staticroute/{{sr.name}}/edit\"><i class=\"icon-edit\"></i></a></td>\n" +
    "        <td>\n" +
    "          <a class=\"row-delete\" href=\"\">\n" +
    "          <i class=\"icon-remove\"></i>\n" +
    "      </a>\n" +
    "    </td>\n" +
    "      </tr>\n" +
    "  </tbody>\n" +
    "  <tfoot class=\"hide-if-no-paging\">\n" +
    "    <tr>\n" +
    "      <td colspan=\"5\">\n" +
    "        <div class=\"pagination pagination-centered\"></div>\n" +
    "      </td>\n" +
    "    </tr>\n" +
    "  </tfoot>\n" +
    "</table>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "<script type=\"text/javascript\">\n" +
    "    \n" +
    "    $('.footable').footable({\n" +
    "      autoLoad: true\n" +
    "    });\n" +
    "</script>");
}]);

angular.module("network/subnets.create.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("network/subnets.create.tpl.html",
    "<div class=\"col-md-4\">\n" +
    " <form role=\"form\" name=\"form\">\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"name\">Name</label>\n" +
    "      <input type=\"text\" class=\"form-control input-sm\" ng-model=\"data.name\" id=\"name\" placeholder=\"Subnet name\" required>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"gateway\">Gateway IP Address/Mask</label>\n" +
    "      <input type=\"text\" class=\"form-control input-sm\" ng-model=\"data.subnet\" id=\"gateway\" placeholder=\"Gateway IP Address/Mask: 10.0.0.1/24\" required>\n" +
    "    </div>\n" +
    "\n" +
    "    <button-submit form='form' function=\"submit\"></button-submit>\n" +
    "    <button-cancel state=\"network.subnets\"></button-cancel>\n" +
    "    <span class=\"error clearfix\">{{ error }}</span>\n" +
    "  </form>\n" +
    "\n" +
    "  <br/>\n" +
    "</div>\n" +
    "");
}]);

angular.module("network/subnets.edit.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("network/subnets.edit.tpl.html",
    "<div class=\"col-md-4\">\n" +
    " <form role=\"form\" name=\"form\">\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"name\">Name</label>\n" +
    "      <input disabled type=\"text\" class=\"form-control input-sm\" ng-model=\"data.name\" id=\"name\" placeholder=\"Subnet name\" required>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"form-group\">\n" +
    "      <label for=\"gateway\">Gateway IP Address/Mask</label>\n" +
    "      <input type=\"text\" class=\"form-control input-sm\" ng-model=\"data.subnet\" id=\"gateway\" placeholder=\"Gateway IP Address/Mask: 10.0.0.1/24\" required>\n" +
    "    </div>\n" +
    "\n" +
    "    <button-submit form='form' function=\"submit\"></button-submit>\n" +
    "    <button-cancel state=\"network.subnets\"></button-cancel>\n" +
    "    <span class=\"error clearfix\">{{ error }}</span>\n" +
    "  </form>\n" +
    "\n" +
    "  <br/>\n" +
    "</div>\n" +
    "");
}]);

angular.module("network/subnets.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("network/subnets.tpl.html",
    "<!--<div class=\"col-md-10\">\n" +
    "  <div class=\"well well-sm\">\n" +
    "    <a href=\"index.html#/network/subnet/create\" class=\"btn btn-xs btn-primary\"><i class=\"icon-plus-sign\"></i> Create</a>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\"form-group\">\n" +
    "    <div class=\"form-inline\">\n" +
    "      <label class=\"control-label\" for=\"search\">Search</label>\n" +
    "\n" +
    "      <div class=\"form-group\">\n" +
    "        <input class=\"form-control input-sm\" id=\"search\" placeholder=\"Search\" type=\"text\" ng-model=\"search\">\n" +
    "      </div>\n" +
    "    </div>\n" +
    "  </div>\n" +
    "\n" +
    "  <table class=\"table table-bordered table-condensed\">\n" +
    "    <thead>\n" +
    "      <tr>\n" +
    "        <th>Name</th>\n" +
    "        <th>Subnet</th>\n" +
    "      </tr>\n" +
    "    </thead>\n" +
    "    <tbody>\n" +
    "      <tr ng-repeat=\"subnet in data.subnetConfig | filter:search\">\n" +
    "        <td>{{subnet.name}}</td>\n" +
    "        <td>{{subnet.subnet}}</td>\n" +
    "      </tr>\n" +
    "    </tbody>\n" +
    "  </table>\n" +
    "</div>\n" +
    "-->\n" +
    "\n" +
    "\n" +
    "<a class=\"btn btn-info add-row\" href=\"index.html#/network/subnet/create\">\n" +
    "    Add Subnet\n" +
    "</a>\n" +
    "\n" +
    "<table class=\"footable table\" data-page-size=\"5\" page-navigation=\"pagination\">\n" +
    "  <thead>\n" +
    "    <tr>\n" +
    "        <th>Name</th>\n" +
    "        <th data-hide=\"phone\">Subnet</th>\n" +
    "    </tr>\n" +
    "  </thead>\n" +
    "  <tbody>\n" +
    "    <tr ng-repeat=\"subnet in data.subnetConfig | filter:search\" on-finish-render>\n" +
    "        <td>{{subnet.name}}</td>\n" +
    "        <td>{{subnet.subnet}}</td>\n" +
    "        <td><a href=\"index.html#/network/subnet/{{subnet.name}}/edit\"><i class=\"icon-edit\"></i></a></td>\n" +
    "        <td>\n" +
    "          <a class=\"row-delete\" href=\"\">\n" +
    "          <i class=\"icon-remove\"></i>\n" +
    "      </a>\n" +
    "    </td>\n" +
    "      </tr>\n" +
    "  </tbody>\n" +
    "  <tfoot class=\"hide-if-no-paging\">\n" +
    "    <tr>\n" +
    "      <td colspan=\"5\">\n" +
    "        <div class=\"pagination pagination-centered\"></div>\n" +
    "      </td>\n" +
    "    </tr>\n" +
    "  </tfoot>\n" +
    "</table>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "<script type=\"text/javascript\">\n" +
    "    \n" +
    "    $('.footable').footable();\n" +
    "</script>");
}]);

angular.module("node/detail.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("node/detail.tpl.html",
    "<!--<h3>Ports</h3>\n" +
    "<table class=\"table table-stripped\">\n" +
    "  <thead>\n" +
    "    <tr>\n" +
    "      <th class=\"col-md-8\">Name</th>\n" +
    "      <th class=\"col-md-2\">Type</th>\n" +
    "      <th class=\"col-md-2\">State</th>\n" +
    "    </tr>\n" +
    "  </thead>\n" +
    "  <tbody>\n" +
    "    <tr class=\"table table-striped\" ng-repeat=\"ncp in data.nodeConnectorProperties | noRootPorts | orderBy:'properties.name.value'\">\n" +
    "      <td>{{ncp.properties.name.value}}</td>\n" +
    "      <td>{{ncp.nodeconnector.type}}</td>\n" +
    "      <td>\n" +
    "        <i class=\"icon-ok-sign\"></i>\n" +
    "        <port-state value=\"{{ncp.properties.state.value}}\"></port-state>\n" +
    "      </td>\n" +
    "    </tr>\n" +
    "  </tbody>\n" +
    "</table>-->\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "<table class=\"footable table\">\n" +
    "  <thead>\n" +
    "    <tr>\n" +
    "      <th>Name</th>\n" +
    "      <th>Type</th>\n" +
    "      <th>State</th>\n" +
    "    </tr>\n" +
    "  </thead>\n" +
    "  <tbody>\n" +
    "    <tr ng-repeat=\"ncp in data.nodeConnectorProperties | noRootPorts | orderBy:'properties.name.value'\">\n" +
    "        <!--<td><input type=\"checkbox\" name=\"select-node-{{item.node.id}}\" ng-click=\"unselect($event)\" id=\"select-node-{{item.node.id}}\" /></td>-->\n" +
    "        <td>{{ncp.properties.name.value}}</td>\n" +
    "        <td>{{ncp.nodeconnector.type}}</td>\n" +
    "        <td>\n" +
    "          <i class=\"icon-ok-sign\"></i>\n" +
    "          <port-state value=\"{{ncp.properties.state.value}}\"></port-state>\n" +
    "        </td>\n" +
    "      </tr>\n" +
    "  </tbody>\n" +
    "</table>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/javascript\">\n" +
    "    $('.footable').footable();\n" +
    "</script>");
}]);

angular.module("node/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("node/index.tpl.html",
    "<!--<ctrl-reload service=\"svc\"></ctrl-reload>\n" +
    "<span>Selected: {{numberSelectedItems}}</span>-->\n" +
    "<!--<div class=\"indexGrid\" ng-grid=\"gridOptions\"></div>-->\n" +
    "\n" +
    "<!--<a class=\"btn btn-info add-row\" href=\"index.html#/node/create\">\n" +
    "    Add New Node\n" +
    "</a>-->\n" +
    "\n" +
    "<table class=\"footable table\">\n" +
    "	<thead>\n" +
    "		<tr>\n" +
    "\n" +
    "			<th>Node ID</th>\n" +
    "			<th data-hide=\"phone\">Node name</th>\n" +
    "			<th data-hide=\"phone\">MAC Address</th>\n" +
    "		</tr>\n" +
    "	</thead>\n" +
    "	<tbody>\n" +
    "		<tr ng-repeat=\"item in data | filter:search\">\n" +
    "      	<!--<td><input type=\"checkbox\" name=\"select-node-{{item.node.id}}\" ng-click=\"unselect($event)\" id=\"select-node-{{item.node.id}}\" /></td>-->\n" +
    "        <td><a href=\"index.html#/node/{{item.node.type}}/{{item.node.id}}/detail\">{{item.node.id}}</a></td>\n" +
    "        <td>{{item.properties.description.value}}</td>\n" +
    "        <td>{{item.properties.macAddress.value}}</td>\n" +
    "        <!--<td>\n" +
    "        	<a class=\"row-delete\" href=\"#\">\n" +
    "    			<i class=\"icon-remove\"></i>\n" +
    "			</a>\n" +
    "		</td>-->\n" +
    "      </tr>\n" +
    "	</tbody>\n" +
    "</table>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/javascript\">\n" +
    "		$('.footable').footable();\n" +
    "</script>\n" +
    "\n" +
    "<!--\n" +
    "<table class=\"table table-striped table-bordered table-condensed\">\n" +
    "    <thead >\n" +
    "      <tr class=\"table-top\">\n" +
    "      	<th class=\"table-checkbox-row\"><input ng-click=\"selectAll()\" type=\"checkbox\" id=\"checkAll\" /></th>\n" +
    "        <th>Node ID</th>\n" +
    "        <th>Node Name</th>\n" +
    "        <th>MAC Address</th>\n" +
    "      </tr>\n" +
    "    </thead>\n" +
    "    <tbody>\n" +
    "      <tr ng-repeat=\"item in data.nodeProperties | filter:search\">\n" +
    "      	<td><input type=\"checkbox\" name=\"select-node-{{item.node.id}}\" ng-click=\"unselect($event)\" id=\"select-node-{{item.node.id}}\" /></td>\n" +
    "        <td>{{item.node.id}}</td>\n" +
    "        <td>{{item.properties.description.value}}</td>\n" +
    "        <td>{{item.properties.macAddress.value}}</td>\n" +
    "      </tr>\n" +
    "    </tbody>\n" +
    "  </table> -->");
}]);

angular.module("node/root.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("node/root.tpl.html",
    "<div class=\"main\" ui-view></div>\n" +
    "");
}]);

angular.module("span_ports/create.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("span_ports/create.tpl.html",
    "");
}]);

angular.module("span_ports/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("span_ports/index.tpl.html",
    "<a class=\"btn btn-info add-row\" href=\"index.html#/span_ports/create\">\n" +
    "    Add Span Port\n" +
    "</a>\n" +
    "Search:\n" +
    "<input id=\"filter\" type=\"text\"></input>\n" +
    "<table class=\"footable table\" data-page-size=\"5\" page-navigation=\"pagination\">\n" +
    "  <thead>\n" +
    "    <tr>\n" +
    "        <th>Node</th>\n" +
    "        <th>SPAN Port</th>\n" +
    "    </tr>\n" +
    "  </thead>\n" +
    "  <tbody>\n" +
    "    <tr ng-repeat=\"sr in data.staticRoute | filter:search\" on-finish-render>\n" +
    "        <td>{{sr.name}}</td>\n" +
    "        <td>{{sr.prefix}}</td>\n" +
    "        <td>\n" +
    "          <a class=\"row-delete\" href=\"\">\n" +
    "          <i class=\"icon-remove\"></i>\n" +
    "      </a>\n" +
    "    </td>\n" +
    "      </tr>\n" +
    "  </tbody>\n" +
    "  <tfoot class=\"hide-if-no-paging\">\n" +
    "    <tr>\n" +
    "      <td colspan=\"5\">\n" +
    "        <div class=\"pagination pagination-centered\"></div>\n" +
    "      </td>\n" +
    "    </tr>\n" +
    "  </tfoot>\n" +
    "</table>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "<script type=\"text/javascript\">\n" +
    "    \n" +
    "    $('.footable').footable();\n" +
    "</script>");
}]);

angular.module("span_ports/root.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("span_ports/root.tpl.html",
    "<div class=\"main\" ui-view></div>\n" +
    "");
}]);

angular.module("topology/topology.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("topology/topology.tpl.html",
    "<div class=\"container\">\n" +
    "  <div class=\"row\">\n" +
    "    <div class=\"col-md-2\">\n" +
    "      <h3>Controls</h3>\n" +
    "      <button class=\"btn btn-primary\" ng-click=\"createTopology()\">Reload</button>\n" +
    "\n" +
    "    </div>\n" +
    "    <topology-simple topology-data=\"topologyData\"></topology-simple>\n" +
    "  </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("user/create.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("user/create.tpl.html",
    "<form role=\"form\" name=\"createForm\">\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"name\">Name</label>\n" +
    "    <input type=\"text\" class=\"form-control input-sm control-label\" ng-model=\"data.user\" id=\"user\" placeholder=\"Username\" required>\n" +
    "  </div>\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"name\">Password</label>\n" +
    "    <input type=\"text\" class=\"form-control input-sm control-label\" ng-model=\"data.password\" id=\"password\" placeholder=\"Password\" required>\n" +
    "  </div>\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"name\">Verify Password</label>\n" +
    "    <input type=\"text\" class=\"form-control input-sm control-label\" ng-model=\"verifyPasword\" id=\"verifypasword\" placeholder=\"Verify Password\" required>\n" +
    "  </div>\n" +
    "\n" +
    "  <div class=\"form-group\">\n" +
    "    <label for=\"nodes\">Nodes</label>\n" +
    "    <select class=\"form-control input-sm\" id=\"roles\" ui-select2 ng-model=\"data.roles\">\n" +
    "      <option ng-repeat=\"role in Roles\" value=\"{{role.name}}\">{{role.name}}</option>\n" +
    "    </select>\n" +
    "  </div>\n" +
    "\n" +
    "\n" +
    "  <button-submit form=\"createForm\" function=\"submit\"></button-submit>\n" +
    "  <button-cancel state=\"user.index\"></button-cancel>\n" +
    "  <span class=\"error clearfix\">{{ error }}</span>\n" +
    "</form>");
}]);

angular.module("user/index.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("user/index.tpl.html",
    "<iframe src=\"http://odl.cloudistic.me:8080/controller/osgi/system/console/bundles\" />\n" +
    "\n" +
    "<a class=\"btn btn-info add-row btn-orange\" href=\"index.html#/flow/create\">\n" +
    "    Add New User\n" +
    "</a>\n" +
    "Search:\n" +
    "<input id=\"filter\" type=\"text\"></input>\n" +
    "<table class=\"footable table\" data-page-size=\"5\" page-navigation=\"pagination\">\n" +
    "	<thead>\n" +
    "		<tr>\n" +
    "			<th data-toggle=\"true\">User</th>\n" +
    "			<th data-hide=\"phone\">Role</th>\n" +
    "		</tr>\n" +
    "	</thead>\n" +
    "	<tbody>\n" +
    "		<tr ng-repeat=\"item in data.flowConfig | filter:search\" on-finish-render>\n" +
    "	        <td>{{item.user.name}}</td>\n" +
    "	        <td>{{item.user.roles}}</td>\n" +
    "	        <td>\n" +
    "	        	<a class=\"row-delete\" href=\"#\">\n" +
    "	    			<i class=\"icon-remove\"></i>\n" +
    "				</a>\n" +
    "			</td>\n" +
    "      	</tr>\n" +
    "	</tbody>\n" +
    "	<tfoot class=\"hide-if-no-paging\">\n" +
    "		<tr>\n" +
    "			<td colspan=\"5\">\n" +
    "				<div class=\"pagination pagination-centered\"></div>\n" +
    "			</td>\n" +
    "		</tr>\n" +
    "	</tfoot>\n" +
    "</table>\n" +
    "<script type=\"text/javascript\">\n" +
    "		$('.footable').footable();\n" +
    "</script>");
}]);

angular.module("user/root.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("user/root.tpl.html",
    "\n" +
    "<!-- For child state use-->\n" +
    "<div ui-view=\"submenu\"></div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"main\" ui-view></div>\n" +
    "");
}]);
