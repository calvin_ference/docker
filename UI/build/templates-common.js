angular.module('templates-common', ['breadcrumb/breadcrumb.tpl.html', 'draw2d/draw2d.tpl.html', 'login/forgot_password.tpl.html', 'login/login.tpl.html', 'login/register.tpl.html', 'navigation/nav_item_template.tpl.html', 'navigation/navigation.tpl.html', 'topbar/messages.tpl.html', 'topbar/notifications.tpl.html', 'topbar/tasks.tpl.html', 'topbar/topbar.tpl.html', 'topbar/user_menu.tpl.html']);

angular.module("breadcrumb/breadcrumb.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("breadcrumb/breadcrumb.tpl.html",
    "<div class=\"breadcrumbs\" id=\"breadcrumbs\">\n" +
    "    <script type=\"text/javascript\">\n" +
    "        try {\n" +
    "            ace.settings.check('breadcrumbs', 'fixed')\n" +
    "        } catch (e) {\n" +
    "        }\n" +
    "    </script>\n" +
    "\n" +
    "    <ul class=\"breadcrumb\">\n" +
    "        <li>\n" +
    "            <i class=\"icon-home home-icon\"></i>\n" +
    "            <a href=\"#\">Home</a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "    <!--.breadcrumb-->\n" +
    "\n" +
    "    <div class=\"nav-search\" id=\"nav-search\">\n" +
    "        <form class=\"form-search\">\n" +
    "			<span class=\"input-icon\">\n" +
    "				<input type=\"text\" placeholder=\"Search ...\" class=\"input-small nav-search-input\" id=\"nav-search-input\"\n" +
    "                       autocomplete=\"off\"/>\n" +
    "				<i class=\"icon-search nav-search-icon\"></i>\n" +
    "			</span>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "    <!--#nav-search-->\n" +
    "\n" +
    "</div>");
}]);

angular.module("draw2d/draw2d.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("draw2d/draw2d.tpl.html",
    "<script src=\"assets/js/fix-old-jquery.js\"></script>\n" +
    "\n" +
    "\n" +
    "\n" +
    "<script src=\"assets/js/draw2d/lib/raphael.js\"></script>\n" +
    "<script src=\"assets/js/draw2d/lib/jquery.layout.js\"></script>\n" +
    "<script src=\"assets/js/draw2d/lib/jquery.autoresize.js\"></script>\n" +
    "\n" +
    "<script src=\"assets/js/draw2d/lib/jquery.contextmenu.js\"></script>\n" +
    "<script src=\"assets/js/draw2d/lib/rgbcolor.js\"></script>\n" +
    "<script src=\"assets/js/draw2d/lib/canvg.js\"></script>\n" +
    "<script src=\"assets/js/draw2d/lib/Class.js\"></script>\n" +
    "<script src=\"assets/js/draw2d/lib/json2.js\"></script>\n" +
    "<script src=\"assets/js/draw2d/src/draw2d.js\"></script>\n" +
    "<script src=\"scripts/js/requirejs-config.js\"></script>\n" +
    "\n" +
    "\n" +
    "<div id=\"draw2d\">\n" +
    "<!-- we need this since the new version of jquery lack this function and draw2d use it -->\n" +
    "\n" +
    "\n" +
    "<div id=\"gfx_holder\"></div>\n" +
    "\n" +
    "<script type=\"text/javascript\">\n" +
    "\n" +
    "    //This function is called when scripts/helper/util.js is loaded.\n" +
    "    //If util.js calls define(), then this function is not fired until\n" +
    "    //util's dependencies have loaded, and the util argument will hold\n" +
    "    //the module value for \"helper/util\".\n" +
    "  \n" +
    "\n" +
    "    //This function is called when scripts/helper/util.js is loaded.\n" +
    "    //If util.js calls define(), then this function is not fired until\n" +
    "    //util's dependencies have loaded, and the util argument will hold\n" +
    "    //the module value for \"helper/util\".\n" +
    "    require([\"util\", \"geo\", \"layout\", \"commandPackage\", \"policy\", \"sourcePackage\", \"shape\", \"decoratorPackage\"], function() {\n" +
    "    //This function is called after some/script.js has loaded.\n" +
    "          var canvas = new draw2d.Canvas(\"gfx_holder\");\n" +
    "      \n" +
    "       \n" +
    "      \n" +
    "      // create and add two nodes which contains Ports (In and OUT)\n" +
    "      //\n" +
    "      var start = new draw2d.shape.node.Start();\n" +
    "      var end   = new draw2d.shape.node.End();\n" +
    "        \n" +
    "     // ...add it to the canvas \n" +
    "     canvas.addFigure( start, 50,50);\n" +
    "     canvas.addFigure( end, 230,80);\n" +
    "           \n" +
    " // Create a Connection and connect the Start and End node\n" +
    "      //\n" +
    "      var c = new draw2d.Connection();\n" +
    "       \n" +
    "      // Set the endpoint decorations for the connection\n" +
    "      //\n" +
    "      c.setSourceDecorator(new draw2d.decoration.connection.BarDecorator());\n" +
    "      c.setTargetDecorator(new draw2d.decoration.connection.DiamondDecorator());   \n" +
    "      // Connect the endpoints with the start and end port\n" +
    "      //\n" +
    "      c.setSource(start.getInputPort(0));\n" +
    "      c.setTarget(end.getOutputPort(0));\n" +
    "            \n" +
    "      // and finally add the connection to the canvas\n" +
    "      canvas.addFigure(c);\n" +
    "    });\n" +
    "</script>\n" +
    "\n" +
    "</div>");
}]);

angular.module("login/forgot_password.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("login/forgot_password.tpl.html",
    "<div id=\"forgot-box\" class=\"forgot-box widget-box no-border\">\n" +
    "    <div class=\"widget-body\">\n" +
    "        <div class=\"widget-main\">\n" +
    "            <h4 class=\"header red lighter bigger\">\n" +
    "                <i class=\"icon-key\"></i>\n" +
    "                Retrieve Password\n" +
    "            </h4>\n" +
    "\n" +
    "            <div class=\"space-6\"></div>\n" +
    "            <p>\n" +
    "                Enter your email and to receive instructions\n" +
    "            </p>\n" +
    "\n" +
    "            <form>\n" +
    "                <fieldset>\n" +
    "                    <label>\n" +
    "															<span class=\"block input-icon input-icon-right\">\n" +
    "																<input type=\"email\" class=\"span12\"\n" +
    "                                                                       data-ng-model=\"recover.email\"\n" +
    "                                                                       placeholder=\"Email\"/>\n" +
    "																<i class=\"icon-envelope\"></i>\n" +
    "															</span>\n" +
    "                    </label>\n" +
    "\n" +
    "                    <div class=\"clearfix\">\n" +
    "                        <button ng-click=\"sendForgotPassword()\" type=\"button\"\n" +
    "                                class=\"width-35 pull-right btn btn-small btn-danger\">\n" +
    "                            <i class=\"icon-lightbulb\"></i>\n" +
    "                            Send Me!\n" +
    "                        </button>\n" +
    "                    </div>\n" +
    "                </fieldset>\n" +
    "            </form>\n" +
    "        </div>\n" +
    "        <!--/widget-main-->\n" +
    "\n" +
    "        <div class=\"toolbar center\">\n" +
    "            <a href=\"#\" onclick=\"show_box('login-box'); return false;\" class=\"back-to-login-link\">\n" +
    "                Back to login\n" +
    "                <i class=\"icon-arrow-right\"></i>\n" +
    "            </a>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <!--/widget-body-->\n" +
    "</div><!--/forgot-box-->");
}]);

angular.module("login/login.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("login/login.tpl.html",
    "\n" +
    "<div class=\"login-panel panel panel-default\">\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <h3 class=\"panel-title\">Please Sign In</h3>\n" +
    "    </div>\n" +
    "    <div style=\"color:Red;text-align: center\">{{error}}</div>\n" +
    "    <img style=\"width: 350px\" src=\"assets/images/opendaylight-open-source.jpg\" alt=\"OpenDayLight\">\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form role=\"form\">\n" +
    "            <fieldset>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <input class=\"form-control\" ng-model=\"login.username\" placeholder=\"Username\" name=\"username\" type=\"text\" autofocus>\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <input class=\"form-control\" placeholder=\"Password\" ng-model=\"login.password\" name=\"password\" type=\"password\" value=\"\">\n" +
    "                </div>\n" +
    "                <div class=\"checkbox\">\n" +
    "                    <label>\n" +
    "                        <input ng-model=\"login.remember\" name=\"remember\" type=\"checkbox\" value=\"Remember Me\">Remember Me\n" +
    "                    </label>\n" +
    "                </div>\n" +
    "                <!-- Change this to a button or input when using this as a form -->\n" +
    "                <button ng-click=\"sendLogin()\" class=\"btn btn-lg btn-orange btn-block\">Login</a>\n" +
    "            </fieldset>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("login/register.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("login/register.tpl.html",
    "<div id=\"signup-box\" class=\"signup-box widget-box no-border\">\n" +
    "                                    <div class=\"widget-body\">\n" +
    "                                        <div class=\"widget-main\">\n" +
    "                                            <h4 class=\"header green lighter bigger\">\n" +
    "                                                <i class=\"icon-group blue\"></i>\n" +
    "                                                New User Registration\n" +
    "                                            </h4>\n" +
    "\n" +
    "                                            <div class=\"space-6\"></div>\n" +
    "                                            <p> Enter your details to begin: </p>\n" +
    "\n" +
    "                                            <form>\n" +
    "                                                <fieldset>\n" +
    "                                                    <label class=\"block clearfix\">\n" +
    "                                                        <span class=\"block input-icon input-icon-right\">\n" +
    "                                                            <input type=\"email\" data-ng-model=\"register.email\" class=\"form-control\" placeholder=\"Email\" />\n" +
    "                                                            <i class=\"icon-envelope\"></i>\n" +
    "                                                        </span>\n" +
    "                                                    </label>\n" +
    "\n" +
    "                                                    <label class=\"block clearfix\">\n" +
    "                                                        <span class=\"block input-icon input-icon-right\">\n" +
    "                                                            <input type=\"text\" data-ng-model=\"register.username\" class=\"form-control\" placeholder=\"Username\" />\n" +
    "                                                            <i class=\"icon-user\"></i>\n" +
    "                                                        </span>\n" +
    "                                                    </label>\n" +
    "\n" +
    "                                                    <label class=\"block clearfix\">\n" +
    "                                                        <span class=\"block input-icon input-icon-right\">\n" +
    "                                                            <input type=\"password\" data-ng-model=\"register.password\" class=\"form-control\" placeholder=\"Password\" />\n" +
    "                                                            <i class=\"icon-lock\"></i>\n" +
    "                                                        </span>\n" +
    "                                                    </label>\n" +
    "\n" +
    "                                                    <label class=\"block clearfix\">\n" +
    "                                                        <span class=\"block input-icon input-icon-right\">\n" +
    "                                                            <input type=\"password\" data-ng-model=\"register.repeatPassword\" class=\"form-control\" placeholder=\"Repeat password\" />\n" +
    "                                                            <i class=\"icon-retweet\"></i>\n" +
    "                                                        </span>\n" +
    "                                                    </label>\n" +
    "\n" +
    "                                                    <label class=\"block\">\n" +
    "                                                        <input type=\"checkbox\" data-ng-model=\"register.userAgreement\" class=\"ace\" />\n" +
    "                                                        <span class=\"lbl\">\n" +
    "                                                            I accept the\n" +
    "                                                            <a href=\"#\">User Agreement</a>\n" +
    "                                                        </span>\n" +
    "                                                    </label>\n" +
    "\n" +
    "                                                    <div class=\"space-24\"></div>\n" +
    "\n" +
    "                                                    <div class=\"clearfix\">\n" +
    "                                                        <button type=\"reset\" class=\"width-30 pull-left btn btn-sm\">\n" +
    "                                                            <i class=\"icon-refresh\"></i>\n" +
    "                                                            Reset\n" +
    "                                                        </button>\n" +
    "\n" +
    "                                                        <button type=\"button\" ng-click=\"sendRegister()\" class=\"width-65 pull-right btn btn-sm btn-success\">\n" +
    "                                                            Register\n" +
    "                                                            <i class=\"icon-arrow-right icon-on-right\"></i>\n" +
    "                                                        </button>\n" +
    "                                                    </div>\n" +
    "                                                </fieldset>\n" +
    "                                            </form>\n" +
    "                                        </div>\n" +
    "\n" +
    "                                        <div class=\"toolbar center\">\n" +
    "                                            <a href=\"#\" onclick=\"show_box('login-box'); return false;\" class=\"back-to-login-link\">\n" +
    "                                                <i class=\"icon-arrow-left\"></i>\n" +
    "                                                Back to login\n" +
    "                                            </a>\n" +
    "                                        </div>\n" +
    "                                    </div><!-- /widget-body -->\n" +
    "                                </div><!-- /signup-box -->");
}]);

angular.module("navigation/nav_item_template.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("navigation/nav_item_template.tpl.html",
    "<a href=\"{{item.link}}\" ng-if=\"isValid(item.submenu)\" target=\"_self\">\n" +
    "    <i class=\"{{item.icon}}\"></i>\n" +
    "    {{item.title}}\n" +
    "    <span ng-show=\"isValid(item.submenu)\" class=\"arrow icon-angle-down\"></span>\n" +
    "</a>\n" +
    "<a href=\"{{item.link}}\" target=\"_self\" ng-if=\"!isValid(item.submenu)\">\n" +
    "    <i class=\"{{item.icon}}\"></i>\n" +
    "    {{item.title}}\n" +
    "</a>\n" +
    "\n" +
    "<ul class=\"nav nav-second-level collapse\" style=\"display: {{display}}\">\n" +
    "\n" +
    "    <li ng-controller=\"navItemCtrl\" ng-repeat=\"item in item.submenu\"\n" +
    "        ng-include=\"'navigation/nav_item_template.tpl.html'\">\n" +
    "\n" +
    "    </li>\n" +
    "\n" +
    "\n" +
    "</ul>\n" +
    "\n" +
    "\n" +
    "  \n" +
    " \n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("navigation/navigation.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("navigation/navigation.tpl.html",
    "<nav class=\"navbar-default navbar-static-side menu-sidebar\" role=\"navigation\">\n" +
    "    <div class=\"sidebar-collapse\">\n" +
    "    <ul id=\"side-menu\" class=\"nav\">\n" +
    "        <!--\n" +
    "\n" +
    "        We don't need the side-search for now...\n" +
    "        <li class=\"sidebar-search\">\n" +
    "            <div class=\"input-group custom-search-form\">\n" +
    "                <input class=\"form-control\" placeholder=\"Search...\" type=\"text\">\n" +
    "                <span class=\"input-group-btn\">\n" +
    "                    <button class=\"btn btn-default\" type=\"button\">\n" +
    "                        <i class=\"icon-search\"></i>\n" +
    "                    </button>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "        </li>          \n" +
    "                   --> \n" +
    "        <li ng-class=\"{ active: isState(item.active) }\" class=\"{{item.class}}\" ng-controller=\"navItemCtrl\" ng-repeat=\"item in navList\"\n" +
    "            ng-include=\"'navigation/nav_item_template.tpl.html'\">\n" +
    "\n" +
    "\n" +
    "        </li>\n" +
    "\n" +
    "    <ul>\n" +
    "</div>\n" +
    "</nav>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("topbar/messages.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("topbar/messages.tpl.html",
    "<li class=\"dropdown\">\n" +
    "    <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"\">\n" +
    "        <i class=\"icon-envelope icon-animated-vertical\"></i>\n" +
    "    </a>\n" +
    "    <ul class=\"dropdown-menu dropdown-messages\">\n" +
    "        <li ng-repeat=\"message in messages.latest\">\n" +
    "            <a href=\"#\">\n" +
    "               <!-- <img src=\"/avatars/{{message.img}}\" class=\"msg-photo\" alt=\"{{message.name}}'s Avatar\"> Do not need it for now -->\n" +
    "               <div>\n" +
    "                    <strong>{{message.name}}</strong>\n" +
    "                    <span class=\"pull-right text-muted\">\n" +
    "                        <em>{{message.time}}</em>\n" +
    "                    </span>\n" +
    "                </div>\n" +
    "                <div>{{message.summary}}</div>\n" +
    "            </a>\n" +
    "        </li>\n" +
    "\n" +
    "        \n" +
    "        <li>\n" +
    "            <a href=\"#\" class=\"text-center\">\n" +
    "                <strong>Read All Messages</strong>\n" +
    "                <i class=\"icon-arrow-right\"></i>\n" +
    "            </a>\n" +
    "        </li>\n" +
    "\n" +
    "    </ul>\n" +
    "</li>");
}]);

angular.module("topbar/notifications.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("topbar/notifications.tpl.html",
    "<li class=\"dropdown\">\n" +
    "    <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"\">\n" +
    "        <i class=\"icon-bell-alt icon-animated-bell\"></i>\n" +
    "        <span class=\"badge badge-important\">{{ notifs.count }}</span>\n" +
    "    </a>\n" +
    "    <ul class=\"dropdown-menu dropdown-alerts\">\n" +
    "        <li ng-repeat=\"notif in notifs.latest\">\n" +
    "            <a href=\"#\">\n" +
    "                \n" +
    "                <div>\n" +
    "                    <i class=\"btn btn-xs no-hover {{notif.iconClass}} {{notif.icon}}\"></i>{{notif.title}}\n" +
    "                    <span class=\"pull-right text-muted small\">4 minutes ago</span>                 \n" +
    "                </div>\n" +
    "            </a>\n" +
    "        </li>\n" +
    "\n" +
    "\n" +
    "        <li>\n" +
    "            <a href=\"#\" class=\"text-center\">\n" +
    "                <strong>See all notifications</strong>\n" +
    "                <i class=\"icon-arrow-right\"></i>\n" +
    "            </a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</li>");
}]);

angular.module("topbar/tasks.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("topbar/tasks.tpl.html",
    "<li class=\"grey\">\n" +
    "    <a data-toggle=\"dropdown\" class=\"dropdown-toggle\" href=\"\">\n" +
    "        <i class=\"icon-tasks\"></i>\n" +
    "        <span class=\"badge badge-grey\">{{ tasks.count }}</span>\n" +
    "    </a>\n" +
    "\n" +
    "    <ul class=\"dropdown-menu dropdown-tasks\">\n" +
    "        <li ng-repeat=\"task in tasks.latest\">\n" +
    "            <a href=\"#\">\n" +
    "                <div>\n" +
    "                    <p><strong>{{ task.title }}</strong>\n" +
    "                    <span class=\"pull-right text-muted\">{{ task.percentage }}%</span></p>\n" +
    "                </div>\n" +
    "                <div class=\"progress progress-striped active\">\n" +
    "\n" +
    "                    <div class=\"progress progress-mini {{task.progressClass}}\">\n" +
    "                        <div style=\"width:{{task.percentage}}%\" aria-valuemax=\"100\" aria-valuemin=\"0\" aria-valuenow=\"{{task.percentage}}\" role=\"progressbar\" class=\"progress-bar {{task.progressBarClass}}\">\n" +
    "                            <span class=\"sr-only\">{{task.percentage}}% Complete</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </a>\n" +
    "        </li>\n" +
    "\n" +
    "        <li>\n" +
    "            <a href=\"#\">\n" +
    "                See tasks with details\n" +
    "                <i class=\"icon-arrow-right\"></i>\n" +
    "            </a>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</li>");
}]);

angular.module("topbar/topbar.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("topbar/topbar.tpl.html",
    "<nav class=\"navbar navbar-default navbar-static-top\" style=\"margin-bottom: 0\" role=\"navigation\">\n" +
    " <!-- the script for the resizing was here -->\n" +
    "\n" +
    "   \n" +
    "<img id=\"logo_opendaylight\" src=\"assets/images/logo_opendaylight.gif\" class=\"img-responsive-v1\" border=\"0\" alt=\"OpenDayLight\" /><img id=\"logo_opendaylight_white\" src=\"assets/images/logo_opendaylight_white.gif\" class=\"img-responsive-v1\" border=\"0\" alt=\"OpenDayLight\" /><img src=\"assets/images/{{logo}}.gif\"  id=\"page_logo\" class=\"img-responsive-v1\" border=\"0\" alt=\"OpenDayLight\" />\n" +
    "\n" +
    "<div style=\"background-color:red;display:inline;width:100%;\"></div>\n" +
    "    \n" +
    "      <ul class=\"nav navbar-top-links navbar-right\">\n" +
    "      	<!--<img src=\"assets/images/User.png\" class=\"right-topbar\" border=\"0\" alt=\"OpenDayLight\" />\n" +
    "      	<img src=\"assets/images/Info.png\" class=\"right-topbar\" border=\"0\" alt=\"OpenDayLight\" />\n" +
    "      	<img src=\"assets/images/OSGI.png\" class=\"right-topbar\" border=\"0\" alt=\"OpenDayLight\" />-->\n" +
    "            <!-- for now no image since we dont use them.... -->\n" +
    "            <!-- \n" +
    "            Don't need them for now...    \n" +
    "            <div data-mc-top-bar-tasks></div>\n" +
    "            <div data-mc-top-bar-notifications></div>\n" +
    "            <div data-mc-top-bar-messages></div>\n" +
    "\n" +
    "            -->\n" +
    "            <!--<div data-mc-top-bar-user-menu></div>-->\n" +
    "      </ul><!-- /.ace-nav -->\n" +
    "\n" +
    "\n" +
    "</nav>");
}]);

angular.module("topbar/user_menu.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("topbar/user_menu.tpl.html",
    "<li class=\"dropdown\">\n" +
    "    <a data-toggle=\"dropdown\" href=\"\" class=\"dropdown-toggle\">\n" +
    "        <img class=\"nav-user-photo\" src=\"assets/images/user.jpg\" alt=\"Jason's Photo\">\n" +
    "        <span class=\"user-info\">\n" +
    "            <small>Welcome,</small> Jason\n" +
    "        </span>\n" +
    "        <i class=\"icon-caret-down\"></i>\n" +
    "    </a>\n" +
    "    <ul class=\"dropdown-menu dropdown-user\">\n" +
    "        <!-- only need logout for now...\n" +
    "\n" +
    "        <li><a href=\"#\"><i class=\"icon-cog\"></i> Settings</a></li>\n" +
    "        <li><a href=\"#\"><i class=\"icon-user\"></i> Profile</a></li>\n" +
    "        <li class=\"divider\"></li>\n" +
    "\n" +
    "        -->\n" +
    "        <li data-ng-click=\"logOut()\"><a href=\"#\"><i class=\"icon-off\"></i> Logout</a></li>\n" +
    "    </ul>\n" +
    "</li>");
}]);
